package main

import (
	"fmt"
	"math"
	"math/rand"
	"sort"
	"time"

	pn "github.com/aquilax/go-perlin"
	rl "github.com/gen2brain/raylib-go/raylib"
)

const (
	gVersion        = "0.9.0b"
	gWindowWidth    = 1200
	gWindowHeight   = 800
	gTotalGuesses   = 10
	gTotalGuessTime = float32(10)
	gNumCrowd       = 100
)

const (
	gameStateMainMenu = 0
	gameStatePlaying  = 1
	gameStatePaused   = 2
	gameStateOver     = 3
	gameStateLoading  = 4
)

type person struct {
	shirtColor rl.Color
	pantColor  rl.Color
	hairColor  rl.Color
	skinColor  rl.Color
	position   rl.Vector2
	shirt      float32
	pants      float32
}

var guessed bool
var xGuess int32
var yGuess int32
var xActual int32
var yActual int32
var score float64
var winner bool
var currentTime float32
var guessTime float32
var gameState int
var personList []person
var npcTex rl.Texture2D
var iconTex rl.Texture2D
var cursorTex rl.Texture2D
var boxTex rl.Texture2D
var guessCount int32
var totalTime float32
var totalCorrect int32
var guessIndexList []int
var currentGuessIndex int
var gameReady bool
var pnoise *pn.Perlin
var disableIconMotion bool

var debug debugInfo

type debugInfo struct {
	pauseTimer bool
}

func drawPerson(tex rl.Texture2D, p person) {
	scale := float32(6)

	// body
	rl.DrawTexturePro(tex, rl.NewRectangle(0, 0, 5, 13), rl.NewRectangle(p.position.X, p.position.Y, 5*scale, 13*scale), rl.NewVector2(0, 0), 0, p.skinColor)
	// shirt
	rl.DrawTexturePro(tex, rl.NewRectangle(5*p.shirt, 4, 5, 4), rl.NewRectangle(p.position.X, p.position.Y+(4*scale), 5*scale, 4*scale), rl.NewVector2(0, 0), 0, p.shirtColor)
	// pants
	rl.DrawTexturePro(tex, rl.NewRectangle(5*p.pants, 8, 5, 5), rl.NewRectangle(p.position.X, p.position.Y+(8*scale), 5*scale, 5*scale), rl.NewVector2(0, 0), 0, p.pantColor)
}

func getRandomColor() rl.Color {
	r := 10 * rand.Intn(25)
	g := 10 * rand.Intn(25)
	b := 10 * rand.Intn(25)

	return rl.NewColor(uint8(r), uint8(g), uint8(b), 255)
}

func getRandomSkinColor() rl.Color {
	i := rand.Intn(10)
	switch i {
	case 0:
		return rl.NewColor(216, 153, 108, 255)
	case 1:
		return rl.NewColor(100, 64, 40, 255)
	case 2:
		return rl.NewColor(233, 222, 160, 255)
	case 3:
		return rl.NewColor(165, 192, 100, 255)
	case 4:
		return rl.NewColor(76, 57, 28, 255)
	case 5:
		return rl.NewColor(167, 118, 45, 255)
	case 6:
		return rl.NewColor(179, 96, 33, 255)
	case 7:
		return rl.NewColor(207, 171, 10, 255)
	case 8:
		return rl.NewColor(239, 203, 85, 255)
	case 9:
		return rl.NewColor(163, 154, 123, 255)
	}

	return rl.White
}

func getRandomX() float32 {
	x := 100 + rand.Intn(gWindowWidth-200)

	return float32(x)
}

func getRandomY() float32 {
	y := 100 + rand.Intn(gWindowHeight-200)

	return float32(y)
}

func tooCloseToNeightbors(vec rl.Vector2) bool {
	for _, p := range personList {
		xDist := vec.X - p.position.X
		yDist := vec.Y - p.position.Y
		dist := math.Sqrt(float64(xDist*xDist + yDist*yDist))

		if dist < 30 {
			return true
		}
	}
	return false
}

func getRandomPosition() rl.Vector2 {
	x := getRandomX()
	y := getRandomY()

	vec := rl.NewVector2(x, y)
	for tooCloseToNeightbors(vec) {
		x := getRandomX()
		y := getRandomY()

		vec = rl.NewVector2(x, y)
	}
	return vec
}

func createPerson() person {
	return person{
		shirtColor: getRandomColor(),
		pantColor:  getRandomColor(),
		position:   getRandomPosition(),
		skinColor:  getRandomSkinColor(),
		shirt:      float32(1 + rand.Intn(2)),
		pants:      float32(1 + rand.Intn(2)),
	}
}

func alreadyGuessed(i int) bool {
	for _, v := range guessIndexList {
		if i == v {
			return true
		}
	}
	return false
}

func getRandomPerson() int {
	index := -1
	for index < 0 || alreadyGuessed(index) {
		index = rand.Intn(gNumCrowd)
	}
	return index
}

func updateGame(t float32) {
	rl.HideCursor()
	rl.DrawTexturePro(boxTex, rl.NewRectangle(0, 0, 9, 17), rl.NewRectangle(0, 0, 9*6, 17*6), rl.NewVector2(0, 0), 0, rl.Red)
	p := personList[currentGuessIndex]
	p.position.X = 12
	p.position.Y = 12
	drawPerson(npcTex, p)

	for _, p := range personList {
		drawPerson(npcTex, p)
	}

	if !guessed {
		rotation := currentTime / 10 * 360.0
		rl.DrawTexturePro(cursorTex, rl.NewRectangle(0, 0, 17, 17), rl.NewRectangle(float32(rl.GetMouseX()), float32(rl.GetMouseY()), 17*8, 17*8), rl.NewVector2(17*4, 17*4), rotation, rl.Red)
		rl.DrawText(fmt.Sprintf("Time: %.fs", gTotalGuessTime-currentTime), 60, 10, 30, rl.Black)
	} else {
		if winner {
			totalCorrect++
		}
		totalTime += currentTime
		guessed = false
		currentTime = 0
		gameState = gameStatePaused
	}

	if !guessed && (rl.IsMouseButtonPressed(rl.MouseLeftButton) || currentTime > gTotalGuessTime) {
		p := personList[currentGuessIndex]
		xActual = int32(p.position.X) + 18
		yActual = int32(p.position.Y) + 42

		guessed = true
		xGuess = rl.GetMouseX()
		yGuess = rl.GetMouseY()

		xDist := xActual - xGuess
		yDist := yActual - yGuess
		score = math.Sqrt(float64(xDist*xDist + yDist*yDist))

		if score < 70 {
			winner = true
		}
		guessTime = currentTime
		if guessTime > gTotalGuessTime {
			guessTime = gTotalGuessTime
		}
	}

	if !debug.pauseTimer {
		currentTime += t
	}
}

func normalize(v, min, max float32) float32 {
	return (v - min) / (max - min)
}

func lerp(v, min, max float32) float32 {
	return min + v*(max-min)
}

func hslToRgb(h, s, l float32) rl.Color {
	c := float32(math.Abs(float64(2*l - 1)))
	c = 1 - c
	c = c * s

	a := h / 60
	a = float32(math.Mod(float64(a), 2) - 1)
	a = float32(math.Abs(float64(a)))

	x := c * (1.0 - a)
	m := l - c/2

	var vec rl.Vector3
	switch {
	case h >= 0.0 && h < 60.0:
		vec.X = c
		vec.Y = x
		vec.Z = 0
	case h >= 60.0 && h < 120.0:
		vec.X = x
		vec.Y = c
		vec.Z = 0
	case h >= 120.0 && h < 180.0:
		vec.X = 0
		vec.Y = c
		vec.Z = x
	case h >= 180.0 && h < 240.0:
		vec.X = 0
		vec.Y = x
		vec.Z = c
	case h >= 240.0 && h < 300.0:
		vec.X = x
		vec.Y = 0
		vec.Z = c
	case h >= 300.0 && h < 360.0:
		vec.X = c
		vec.Y = 0
		vec.Z = x
	}
	vec.X = vec.X + m
	vec.Y = vec.Y + m
	vec.Z = vec.Z + m

	return rl.NewColor(uint8(vec.X*255), uint8(vec.Y*255), uint8(vec.Z*255), 255)
}

func rotateColor(val int32) rl.Color {
	h := float32(val % 360.0)
	s := float32(1)
	l := float32(0.5)
	return hslToRgb(h, s, l)
}

func updateMainMenu(t float32) {
	rl.ShowCursor()
	xCenter := int32(gWindowWidth / 2)
	yCenter := int32(gWindowHeight / 2)
	buttonColor := rl.RayWhite
	if rl.CheckCollisionPointRec(rl.GetMousePosition(), rl.NewRectangle(float32(xCenter-100), float32(yCenter+100), 200, 100)) {
		buttonColor = rl.Orange
		if rl.IsMouseButtonPressed(rl.MouseLeftButton) {
			gameState = gameStateLoading
			currentTime = 0
			gameReady = false
		}
	}

	scaleLerpValue := normalize(float32(pnoise.Noise1D(float64(currentTime))), -1.0, 1.0)
	rotLerpValue := normalize(float32(pnoise.Noise1D(float64(1+currentTime*0.2))), -1.0, 1.0)
	scaleFactor := lerp(scaleLerpValue, 0, 2)
	rotationAngle := lerp(rotLerpValue, -20, 20)

	if disableIconMotion {
		scaleFactor = 1.0
		rotationAngle = 0.0
	}

	color := rotateColor(int32(currentTime * 30.0))

	iconWidth := float32(630) * scaleFactor
	iconHeight := float32(130) * scaleFactor

	rl.DrawTexturePro(iconTex, rl.NewRectangle(0, 0, 63, 13), rl.NewRectangle(float32(xCenter), float32(yCenter-155), iconWidth, iconHeight), rl.NewVector2(iconWidth/2, iconHeight/2), rotationAngle, color)
	rl.DrawText(fmt.Sprintf("Find %v people in the crowd. Careful, you only have %.fs to find each person before moving on!", gTotalGuesses, gTotalGuessTime), xCenter-460, yCenter+15, 20, rl.RayWhite)
	rl.DrawRectangle(xCenter-100, yCenter+100, 200, 100, buttonColor)
	rl.DrawText("GO", xCenter-20, yCenter+135, 30, rl.Black)

	rl.DrawText(fmt.Sprintf("a game by @yacklebeam, v%v", gVersion), 10, gWindowHeight-30, 20, rl.RayWhite)
	rl.DrawText("press 'p' to toggle menu motion", gWindowWidth-330, gWindowHeight-30, 20, rl.RayWhite)

	currentTime += t
}

func updatePausedScreen(t float32) {
	rl.HideCursor()
	if currentTime > 1 {
		guessCount++
		currentTime = 0
		winner = false
		currentGuessIndex = getRandomPerson()
		gameState = gameStatePlaying

		if guessCount == gTotalGuesses {
			gameState = gameStateOver
		}
	}

	text := "Wrong!"
	color := rl.Red
	x := float32(17)
	if winner {
		text = "Right!"
		color = rl.Green
		x = 34
	}
	rl.DrawText(fmt.Sprintf("%v Time: %.2fs", text, guessTime), 60, 10, 30, color)
	rl.DrawTexturePro(boxTex, rl.NewRectangle(0, 0, 9, 17), rl.NewRectangle(0, 0, 9*6, 17*6), rl.NewVector2(0, 0), 0, color)
	p := personList[currentGuessIndex]
	drawPerson(npcTex, p)
	p.position.X = 12
	p.position.Y = 12
	drawPerson(npcTex, p)

	rl.DrawTexturePro(cursorTex, rl.NewRectangle(x, 0, 17, 17), rl.NewRectangle(float32(xGuess), float32(yGuess), 17*8, 17*8), rl.NewVector2(17*4, 17*4), 0, color)

	currentTime += t
}

func updateGameOverScreen(t float32) {
	rl.ShowCursor()
	xCenter := int32(gWindowWidth / 2)
	yCenter := int32(gWindowHeight / 2)
	buttonColor := rl.RayWhite
	if rl.CheckCollisionPointRec(rl.GetMousePosition(), rl.NewRectangle(float32(xCenter-100), float32(yCenter-50), 200, 100)) {
		buttonColor = rl.Orange
		if rl.IsMouseButtonPressed(rl.MouseLeftButton) {
			gameState = gameStateMainMenu
		}
	}
	avgTime := totalTime / gTotalGuesses
	totalScore := float32(gTotalGuessTime-avgTime) * float32(totalCorrect)
	rl.DrawText(fmt.Sprintf("SCORE: %.2f", totalScore), xCenter-120, yCenter-150, 30, rl.Orange)
	rl.DrawText(fmt.Sprintf("CORRECT: %v / %v", totalCorrect, gTotalGuesses), xCenter-120, yCenter-120, 30, rl.Orange)
	rl.DrawText(fmt.Sprintf("AVG TIME: %.2fs", avgTime), xCenter-120, yCenter-90, 30, rl.Orange)
	rl.DrawRectangle(xCenter-100, yCenter-50, 200, 100, buttonColor)
	rl.DrawText("MAIN MENU", xCenter-90, yCenter-15, 30, rl.Black)
}

func updateLoadingGameScreen(t float32) {
	rl.HideCursor()
	if !gameReady {
		resetGame()
	}
	xCenter := int32(gWindowWidth / 2)
	yCenter := int32(gWindowHeight / 2)
	timeLeft := int32(math.Ceil(float64(5.0 - currentTime)))
	rl.DrawText(fmt.Sprintf("LOADING...%vs", timeLeft), xCenter-90, yCenter-15, 30, rl.Black)
	rl.DrawText("Look here for your target...", 60, 10, 30, rl.Black)
	rl.DrawTexturePro(boxTex, rl.NewRectangle(0, 0, 9, 17), rl.NewRectangle(0, 0, 9*6, 17*6), rl.NewVector2(0, 0), 0, rl.Red)
	rl.DrawTexturePro(cursorTex, rl.NewRectangle(0, 0, 17, 17), rl.NewRectangle(float32(rl.GetMouseX()), float32(rl.GetMouseY()), 17*8, 17*8), rl.NewVector2(17*4, 17*4), 0, rl.Red)
	currentTime += t
	if currentTime > 5.0 {
		currentTime = 0
		gameState = gameStatePlaying
	}
}

func resetGame() {
	guessed = false
	xGuess = 0
	yGuess = 0
	xActual = 0
	yActual = 0
	score = 0
	winner = false
	currentTime = 0
	guessTime = 0
	guessCount = 0
	totalTime = 0
	totalCorrect = 0
	currentGuessIndex = 0

	guessIndexList = nil
	personList = nil

	for i := 0; i < gNumCrowd; i++ {
		p := createPerson()
		personList = append(personList, p)
	}

	// sort crowd by y index
	sort.Slice(personList, func(i, j int) bool {
		return personList[i].position.Y < personList[j].position.Y
	})

	gameReady = true
}

func main() {
	rl.InitWindow(gWindowWidth, gWindowHeight, "CROWDED by yacklebeam")
	rand.Seed(time.Now().UnixNano())

	pnoise = pn.NewPerlin(2., 2., 3, 100)
	disableIconMotion = false

	debug = debugInfo{}

	gameState = gameStateMainMenu

	npcTex = rl.LoadTexture("texture.png")
	iconTex = rl.LoadTexture("icon.png")
	cursorTex = rl.LoadTexture("cursor.png")
	boxTex = rl.LoadTexture("box.png")

	for !rl.WindowShouldClose() {
		t := rl.GetFrameTime()
		rl.BeginDrawing()
		if rl.IsKeyPressed(rl.KeyP) {
			disableIconMotion = !disableIconMotion
		}
		if rl.IsKeyPressed(rl.KeyO) {
			debug.pauseTimer = !debug.pauseTimer
		}
		switch gameState {
		case gameStateMainMenu:
			rl.ClearBackground(rl.Black)
			updateMainMenu(t)
		case gameStatePlaying:
			rl.ClearBackground(rl.RayWhite)
			updateGame(t)
		case gameStatePaused:
			rl.ClearBackground(rl.RayWhite)
			updatePausedScreen(t)
		case gameStateOver:
			rl.ClearBackground(rl.Black)
			updateGameOverScreen(t)
		case gameStateLoading:
			rl.ClearBackground(rl.RayWhite)
			updateLoadingGameScreen(t)
		}

		rl.EndDrawing()
	}

	rl.CloseWindow()
}

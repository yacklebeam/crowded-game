# Crowded

A game by yacklebeam

## The Game
Crowded is a simple "Where's Waldo" game - in a crowd of colorful people, find your target.

After 10 rounds, you'll be scored on the number of people found and the average speed to find them.

Careful! You only get 10s per search before time runs out!

## Install
1) Get latest release from tags page - https://gitlab.com/yacklebeam/crowded-game/-/tags
2) Unzip, then double click run.exe to play